#include "Question.h"

Question::Question(string answer1, string answer2, string answer3, string answer4, string question, int id, int currectAnswer)
{
	this->_question = question;
	this->_id = id;
	this->_answer[0] = currectAnswer;
	this->_answer[1] = answer2;
	this->_answer[2] = answer3;
	this->_answer[3] = answer4;
	this->_currectAnswerIndex = 0;
}
Question::~Question()
{
}
string Question::getQuestion()
{
	return this->_question;
}
string* Question::getAnswer()
{
	return this->_answer;
}
int Question::getCurrectAnswerIndex()
{
	return this->_currectAnswerIndex;
}
int Question::getId()
{
	return this->_id;
}