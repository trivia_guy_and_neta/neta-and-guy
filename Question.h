#include <iostream>
#include <string.h>

using namespace std;

class Question
{
public:
	Question(string answer1, string answer2, string answer3, string answer4, string question, int id, int currectAnswer);
	~Question();
	string getQuestion();
	string* getAnswer();
	int getCurrectAnswerIndex();
	int getId();
	
private:
	string _question;
	string _answer[4];
	int _currectAnswerIndex;
	int _id;
};