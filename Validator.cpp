#include "Validator.h"
#define ONE 1
#define ZERO 0 
#define FOUR 4

bool Validator::isPasswordValid(string password)
{
	int i = ZERO;
	if (password.length() < FOUR)
	{
		return false;
	}

	if (password.find(" "))
	{
		return false;
	}

	while (i < password.length())
	{
		if (!isdigit(password[i]))
		{
			return false;
		}
	}


	i = ZERO;
	while (i < password.length())
	{
		if (!isupper(password[i]))
		{
			return false;
		}

	}

	i = ZERO;
	while (i < password.length())
	{
		if (!islower(password[i]))
		{
			return false;
		}

	}
	return true;
}

bool Validator::isUserNameValid(string userName)
{
	if  (isdigit(userName[ZERO]))
	{
		return false;
	}

	if (userName.length() < ONE)
	{
		return false;
	}

	if (userName.find(" "))
	{
		return false;
	}

	return true;
}
