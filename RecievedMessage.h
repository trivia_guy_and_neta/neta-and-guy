#include <iostream>
#include <string.h>
#include "User.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);
	SOCKET getSock();
	User* getUser();
	void setSock(SOCKET sock);
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _massageCode;
	vector <string> _values;

};


